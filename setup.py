from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Análisis de imágenes relacionadas con textiles con diseños de pueblos originarios con la finalidad de identificar apropiación de estilos y elementos relacionados con la publicidad de los mismos en la industria de la moda internacional, y su comunicación mediante un Twitter Bot.',
    author='Centro ITAM para Datos Algoritmos y Sociedad',
    license='MIT',
)
