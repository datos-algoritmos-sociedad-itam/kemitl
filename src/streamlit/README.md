**inside ec2 instance:**

path: `/home/admin` is `~`
             

create `~/.env` with `ACCESS_KEY`, `AWS_SECRET_ACCESS_KEY`,`BUCKET` (`AWS KEYS` are from  `kube-cdas-itam user`)

**make sure bucket is within policy for listing, geting, deleting permissions**

create `~/.streamlit/config.toml` 


# build

```
docker build 1.8.1/ -t cdas_streamlit/streamlit:1.8.1
```

# run

```
docker build 1.8.1/ -t cdas_streamlit/streamlit:1.8.1
docker run -v /home/admin/:/root/ --rm -d -p 8501:8501 --name streamlit cdas_streamlit/streamlit:1.8.1
```

# start

```
docker exec -it streamlit bash -c "python3 src/start_model_rekog_benchmark.py"

docker exec -it streamlit bash -c "python3 src/start_model_rekog_crop.py"
```
# stop 

```
docker exec -it streamlit bash -c "python3 src/stop_model_rekog_benchmark.py"

docker exec -it streamlit bash -c "python3 src/stop_model_rekog_crop.py"
```
