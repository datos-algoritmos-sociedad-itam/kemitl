Para trabajar soluciones a los retos 2 y 5, el proyecto Kemitl busca desarrollar un mecanismo que, con ayuda de la Ciencia de Datos, permita:

1. Detectar prendas en retailers que tengan inspiración clara en patrones textiles de pueblos originarios y que no estén amparados de ningún convenio o acuerdo internacional, sea colectivo o individual.
2. Comunicar el precio (que es parte de la metadata) con el que se comercializa el artículo de la tienda de high o fast fashion para compararlo con el precio ofertado por el artesano o colectivo de artesanos. El comparativo se relizará a través de posts de Twitter con el hashtag #NoAlRegateo.
3. Buscar tweets con imágenes usando hashtags conocidos respecto al plagio textil. Posteriormente, usaremos la técnica de bag-of-words sobre estos tweets para poder identificar las palabras clave de los tweets relevantes al problem domain y así, realizar la búsqueda con ellas.

