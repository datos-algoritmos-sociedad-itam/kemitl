import datetime
import os
import time
import io
import hashlib
import requests
from io import BytesIO
import json
from collections import Counter
import urllib3

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import cv2
from PIL import Image
from dotenv import load_dotenv
import streamlit as st
import boto3
from botocore.exceptions import ClientError
import imutils

def md5_for_img(img, block_size=256*128):
    """
    Block size directly depends on the block size of your filesystem
    to avoid performances issues
    Here I have blocks of 4096 octets (Default NTFS)
    See:
    https://stackoverflow.com/questions/1131220/get-md5-hash-of-big-files-in-python
    """
    md5 = hashlib.md5()
    for chunk in iter(lambda: img.read(block_size), b''):
         md5.update(chunk)
    return md5.hexdigest()

def image_input():
    #style_model_path = style_models_dict[style_model_name]

    #model = get_model_from_path(style_model_path)

    content_file = st.sidebar.file_uploader(
        "Choose a Content Image", type=["png", "jpg", "jpeg"])

    if content_file is not None:
        content_img = Image.open(content_file)
        content = np.array(content_img)  # pil to cv
        content = cv2.cvtColor(content, cv2.COLOR_RGB2BGR)
    else:
        st.warning("Upload an Image OR Untick the Upload Button")
        st.stop()
    width,height = content_img.size

    WIDTH = st.sidebar.select_slider(
        'QUALITY (May reduce the speed)', list(range(150, width, 50)), value=200)
    #content = imutils.resize(content, width=WIDTH) #np.ndarray
    content = imutils.resize(content, width=WIDTH, height=height) #np.ndarray
    st.sidebar.image(content, width=300, channels='BGR')
    st.image(content, channels='BGR', clamp=True)
    #see: https://pillow.readthedocs.io/en/stable/handbook/concepts.html#concept-modes
    #see: https://pillow.readthedocs.io/en/stable/reference/Image.html?highlight=fromarray#PIL.Image.fromarray
    content_res = Image.fromarray(content, mode="RGB") #PIL.Image.Image
    return (content_res, content_img.format)


def show_logos(logos, basewidth=300):
    resized_logos = []
    for logo_paths in logos:
        logo = Image.open(logo_paths)
        percent_logo = (basewidth/float(logo.size[0]))
        hsize = int((float(logo.size[1])*float(percent_logo)))
        resized_logos.append(logo.resize((basewidth, hsize), Image.ANTIALIAS))
    return resized_logos

def analyze_s3_image(model,bucket,photo, region):
#Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#PDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-rekognition-custom-labels-developer-guide/blob/master/LICENSE-SAMPLECODE.)
    """
    Analyzes an image stored in the specified S3 bucket.
    :param st: streamlit object instance
    :param rek_client: The Amazon Rekognition Boto3 client.
    :param s3_connection: The Amazon S3 Boto3 S3 connection object.
    :param model: The ARN of the Amazon Rekognition Custom Labels model that you want to use.
    :param bucket: The name of the S3 bucket that contains the image that you want to analyze.
    :param photo: The name of the photo that you want to analyze.
    :param min_confidence: The desired threshold/confidence for the call.
    """

    try:
        s3_connection = boto3.resource('s3')
        rek_client=boto3.client('rekognition', region_name=region)
        min_confidence=0.25
        #Get image from S3 bucket.
        st.write("analyzing bucket: %s image: %s", bucket, photo)
        s3_object = s3_connection.Object(bucket,photo)
        s3_response = s3_object.get()

        stream = io.BytesIO(s3_response['Body'].read())
        image=Image.open(stream)

        image_type=Image.MIME[image.format]

        if (image_type == "image/jpeg" or image_type== "image/png") == False:
            st.write("Invalid image type for %s", photo)
            raise ValueError(
                f"Invalid file format. Supply a jpeg or png format file: {photo}")

        img_width, img_height = image.size
        #Call DetectCustomLabels
        response = rek_client.detect_custom_labels(Image={'S3Object': {'Bucket': bucket, 'Name': photo}},
            MinConfidence=min_confidence,
            ProjectVersionArn=model)

        label_count = len(response['CustomLabels'])
        st.write(f"Custom labels detected: {label_count}")
        return response

    except ClientError as client_err:
        st.write("A service client error occurred: " + format(client_err.response["Error"]["Message"]))

    except ValueError as value_err:
        st.write("A value error occurred: " + format(value_err))
    except FileNotFoundError as file_error:
        st.write("File not found error: " + format (file_error))

    except Exception as err:
        st.write("An error occurred: " + format(err))

def return_class_count(train=True):
    if train:
        try:
            # Opening JSON file
            f = open('src/benchmark/train.manifest')
            # returns JSON object as a dictionary
            manifest_train = f.readlines()
            # save manifest variables
        finally:
            f.close()
        dictionary_list_train = []
        for dict_manifest in manifest_train:
            dictionary_list_train.append(
                json.loads(dict_manifest.replace('\n', '')))
        llaves_train = []
        for i in range(len(dictionary_list_train)):
            llaves_train.append(list(dictionary_list_train[i].keys())[
                                1].replace('traindataset-classification_', ''))
        images_class_count = Counter(llaves_train)
    else:
        try:
            # Opening JSON file
            f = open('src/benchmark/test.manifest')
            # returns JSON object as a dictionary
            manifest_test = f.readlines()
            # save manifest variables
        finally:
            f.close()
        dictionary_list_test = []
        for dict_manifest in manifest_test:
            dictionary_list_test.append(
                json.loads(dict_manifest.replace('\n', '')))
        llaves_test = []
        for i in range(len(dictionary_list_test)):
            llaves_test.append(list(dictionary_list_test[i].keys())[
                               1].replace('testdataset-classification_', ''))
        images_class_count = Counter(llaves_test)
    clases_list = []
    num_class_list = []
    for clase in images_class_count:
        clases_list.append(clase)
        num_class_list.append(images_class_count[clase])
    clases_list = np.array(clases_list)
    num_class_list = np.array(num_class_list)
    return clases_list, num_class_list


if __name__ == '__main__':
    path_logos = ['src/figs/LogoCDAS.png', 'src/figs/logo-ITAM.png']
    # para modelo benchmark
    try:
        # Opening JSON file
        f = open('src/benchmark/trained_model_variables.json')
        # returns JSON object as a dictionary
        data = json.load(f)
        # save manifest variables
        rekog_proj_arn = data['rekog_proj_arn']
        rekog_model_arn = data['rekog_model_arn']
    finally:
        f.close()
    # para modelo crop
    try:
        # Opening JSON file
        f_crop = open('src/crop/trained_model_variables.json')
        # returns JSON object as a dictionary
        data_crop = json.load(f_crop)
        # save manifest variables
        rekog_proj_arn_crop = data['rekog_proj_arn']
        rekog_model_arn_crop = data['rekog_model_arn']
    finally:
        f_crop.close()
    #menu de opciones principales
    option = st.sidebar.selectbox(
        "Menú de opciones",
        ("Inicio", "Analizar imagen",
         "Consultar el dataset", "Repositorio", "Contacto")
    )
    # HOME
    if option == "Inicio":
        st.markdown('# Kemitl')
        with open('src/introduccion.txt') as f:
            lines = f.readlines()
        concatenated_strings = "".join(lines)
        st.markdown(concatenated_strings)
        st.subheader('Objetivos')
        with open('src/descripcion_proyecto.txt') as f:
            lines = f.readlines()
        concatenated_strings = "".join(lines)
        st.markdown(concatenated_strings)
        st.image('src/figs/huichol.png')
        st.image('src/figs/otomi.png')
        st.image('src/figs/maya.png')
        st.subheader('Siguientes pasos')
        with open('src/siguientes_pasos.txt') as f:
            lines = f.readlines()
        concatenated_strings = "".join(lines)
        st.markdown(concatenated_strings)
        st.subheader('Definiciones y criterios')
        with open('src/definiciones_criterios.txt') as f:
            lines = f.readlines()
        concatenated_strings = "".join(lines)
        st.markdown(concatenated_strings)
        logo_cdas, logo_itam = show_logos(path_logos)[0],show_logos(path_logos)[1]
        st.image([logo_itam, logo_cdas])


    # Analizar imagen
    dotenv_path = os.path.join(os.path.expanduser('~'), '.env')
    load_dotenv(dotenv_path)
    ACCESS_KEY = os.environ.get("ACCESS_KEY", "")
    AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY", "")
    BUCKET = os.environ.get("BUCKET", "")


    if option == "Analizar imagen":
        st.title("Análisis de imagen")
        option_model = st.selectbox('modelos',
                              ('modelo benchmark', 'modelo con imágenes recortadas',))
        if option_model == 'modelo benchmark':
            st.markdown('### Modelo benchmark')
            img, format = image_input()
            info = st.info('envia tu imagen para poder analizarla')
            guardar = st.button('guardar imagen', key=None, help=None,
                                on_click=None, args=None, kwargs=None, disabled=False)
            if guardar:
                dst = "streamlit"
                date_today = datetime.date.today().strftime("%d-%m-%Y")
                #see: https://stackoverflow.com/questions/46204514/uploading-pil-image-object-to-amazon-s3-python
                in_mem_file = io.BytesIO()
                img.save(in_mem_file, format=format)
                in_mem_file.seek(0)
                info.empty()
                s3 = boto3.client("s3", aws_access_key_id=ACCESS_KEY, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                img_md5 = md5_for_img(in_mem_file)
                key = os.path.join(dst, date_today, img_md5 + "." + format)
                img.save(in_mem_file, format=format)
                in_mem_file.seek(0)
                s3.upload_fileobj(in_mem_file, BUCKET, key)
                st.balloons()
                success = st.success('La imagen se ha enviado de forma exitosa')
                time.sleep(2) # esperamos unos segundos para mostrar el mensaje de que se esta analyzando la imagen
                success.empty()
                info = st.info('El análisis de tu imagen esta empezando...')
                time.sleep(3)
                info.empty()
                # analyze image
                region = "us-east-1"
                try:
                    response_s3 = analyze_s3_image(rekog_model_arn, BUCKET,
                                                   key, region)
                    st.markdown('# Resultados:')
                    st.write(response_s3)
                except Exception as e:
                    st.write(e)
                time.sleep(10)
                st.write("Borrando la imagen")
                s3.delete_object(Bucket=BUCKET, Key=key)
        if option_model == 'modelo con imágenes recortadas':
            st.markdown('### Modelo de imágenes recortadas')
            img, format = image_input()
            info = st.info('envia tu imagen para poder analizarla')
            guardar = st.button('guardar imagen', key=None, help=None,
                                on_click=None, args=None, kwargs=None, disabled=False)
            if guardar:
                dst = "streamlit"
                date_today = datetime.date.today().strftime("%d-%m-%Y")
                #see: https://stackoverflow.com/questions/46204514/uploading-pil-image-object-to-amazon-s3-python
                in_mem_file = io.BytesIO()
                img.save(in_mem_file, format=format)
                in_mem_file.seek(0)
                info.empty()
                s3 = boto3.client("s3", aws_access_key_id=ACCESS_KEY, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
                img_md5 = md5_for_img(in_mem_file)
                key = os.path.join(dst, date_today, img_md5 + "." + format)
                img.save(in_mem_file, format=format)
                in_mem_file.seek(0)
                s3.upload_fileobj(in_mem_file, BUCKET, key)
                st.balloons()
                success = st.success('La imagen se ha enviado de forma exitosa')
                time.sleep(2) # esperamos unos segundos para mostrar el mensaje de que se esta analyzando la imagen
                success.empty()
                info = st.info('El análisis de tu imagen esta empezando...')
                time.sleep(3)
                info.empty()
                # analyze image
                region = "us-east-1" 
                try:
                    response_s3 = analyze_s3_image(rekog_model_arn_crop, BUCKET,
                                                   key, region)
                    st.markdown('# Resultados:')
                    st.write(response_s3)
                except Exception as e:
                    st.write(e)
                time.sleep(10)
                st.write("Borrando la imagen")
                s3.delete_object(Bucket=BUCKET, Key=key)

    if option == "Consultar el dataset":
        st.title("Descargar el dataset")
        st.markdown("#### [Descargar el dataset](http://kemitl-2021-cdas-project-rekognition.s3-website-us-east-1.amazonaws.com/)")
        st.write("#")

        option_sb = st.selectbox('información sobre el dataset',
                              ('descripción del data set','datos de entrenamiento y prueba'))
        col1, col2 = st.columns(2)
        clase_train, count_train = return_class_count(train=True)
        clase_test, count_test = return_class_count(train=False)
        d = {'clase': list(clase_train),
                 'total_datos': list(count_train)}
        df_train = pd.DataFrame(data=d)
        d = {'clase': list(clase_test),'total_datos': list(count_test)}
        df_test = pd.DataFrame(data=d)
        suma_train = np.sum(np.array(df_train.total_datos))
        suma_test = np.sum(np.array(df_test.total_datos))
        suma_total = suma_train+suma_test
        porc_train = round((suma_train/suma_total), 2)*100
        porc_test = round((suma_test/suma_total), 2)*100
        d = {'clase': list(clase_train), 'count': count_train+count_test}
        df_total = pd.DataFrame(data=d)
        # descripcion del data set completo
        if option_sb == 'descripción del data set':
            st.dataframe(df_total)
            plt.style.use('fivethirtyeight')
            fig, ax = plt.subplots(figsize=(10, 20))
            ax.barh(clase_test, count_train+count_test, color='#FFC0CB')
            st.markdown('#')
            st.markdown('### distribución de los datos')
            st.pyplot(fig)
        # mas informacion sobre el set de train y test
        if option_sb == 'datos de entrenamiento y prueba':
            with col1:
                st.dataframe(df_train)
                st.metric(
                    label="set de entrenamiento", value=f"{suma_train}")
                st.metric(
                    label="", value=f"{porc_train}%")

            with col2:
                st.dataframe(df_test)
                st.metric(
                    label="set de prueba", value=f"{suma_test}")
                st.metric(
                    label="", value=f"{porc_test}%")
            plt.style.use('fivethirtyeight')
            fig, ax = plt.subplots(figsize=(10, 30))
            ax.barh(clase_train, count_train,color='#FFB7C5')
            st.markdown('### Datos de entrenamiento')
            st.pyplot(fig)
            fig, ax = plt.subplots(figsize=(10, 30))
            ax.barh(clase_test, count_train,color='#FFB3DE')
            st.markdown('### Datos de prueba')
            st.pyplot(fig)

    if option == 'Repositorio':

        st.markdown(
            '### [Centro ITAM para Datos Algoritmos y Sociedad](https://gitlab.com/datos-algoritmos-sociedad-itam)')
        st.image(
            'https://gitlab.com/uploads/-/system/group/avatar/7093123/LogoCDAS.png?width=64')

        st.write(
            '[Repositorio de Kemitl](https://gitlab.com/datos-algoritmos-sociedad-itam/kemitl)')

        with st.expander("Web scraping en Google Imágenes ", expanded=False):
            st.markdown(
                """
        - Para la obtención de datos de pueblos originarios realizamos _[web scrapping](https://en.wikipedia.org/wiki/Web_scraping)_ en Google Imágenes:
            - [Código fuente](https://gitlab.com/datos-algoritmos-sociedad-itam/kemitl/-/tree/master/references/google_image_scraping)
            - [Aquí la documentación.](https://hackmd.io/VInRskw-SyKr8cbA-uOhfw)
                """
            )
        with st.expander("Web Scraping a sitios de moda acusados de apropiacion cultural ", expanded=False):
            st.markdown(
                """
        - Diversas marcas de fast fashion y high fashion han sido señaladas de realizar apropiación cultural (Zara, Mara Hoffmann, Pineda Covalin y Zimmermann, Carolina Herrera). Para la obtención de datos para comprobar si hay o no
            un uso desleal de patrones textiles de pueblos originarios, realizamos un _[web scrapping](https://en.wikipedia.org/wiki/Web_scraping)_ a sitios de estas marcas.
            - [Código fuente](https://gitlab.com/datos-algoritmos-sociedad-itam/kemitl/-/blob/master/AWS/kemitl-2021-cdas-project/notebooks/scraping/scrape_moda.ipynb)
            - [Aquí la documentación.](https://hackmd.io/6m0kPoBYTgWe7xeD5aOo4g)
                """
            )
        with st.expander("Amazon Rekognition ", expanded=False):
            st.markdown(
                """
        - Utilizamos el servicio de _[Amazon Rekognition](https://aws.amazon.com/es/rekognition/)_ para realizar la tarea de clasificación de imágenes utilizando como categorías los nombres de pueblos originarios y los sitios de moda (no originario)
            - [Código fuente](https://gitlab.com/datos-algoritmos-sociedad-itam/kemitl/-/tree/master/src/models)
            - [Aquí la documentación.](https://hackmd.io/@GrUvIT3yT4OSVu8GCUoDxQ/S1InJTaL5)
                """
            )
    if option == 'Contacto':
        logo_cdas = show_logos(path_logos, basewidth=400)[0]
        st.image([logo_cdas])
        st.markdown('#')
        st.subheader('Síguenos en nuestras redes sociales')
        col1, col2 = st.columns(2)
        with col1:
            logo_social_media_1 = show_logos(
                ["src/figs/Instagram_Glyph_Gradient_RGB.png"], basewidth=100)[0]
            st.image(logo_social_media_1)
        with col2:
            st.markdown(
                '### [@dataalgosocitam](https://www.instagram.com/dataalgosocitam/)')
        st.markdown('#')
        col1, col2 = st.columns(2)
        with col1:
            logo_social_media_1 = show_logos(
                ["src/figs/Twitter_social_icons_circle_blue.png"], basewidth=100)[0]
            st.image(logo_social_media_1)
        with col2:
            st.markdown(
                '### [@DataAlgoSocITAM](https://twitter.com/dataalgosocitam)')
        st.markdown('#')
        st.subheader("Equipo de desarrollo")
        github_cuentas = ['https://github.com/Rebeca98',
                          'https://github.com/LornartheBreton',
                          'https://github.com/Juanes8',
                          'https://github.com/palmoreck']

        st.markdown(
            f'Erick Palacios Moreno - Github [@palmoreck]({github_cuentas[3]})')
        st.markdown(
            f'Rebeca Estephania Angulo Rojas - Github: [@Rebeca98]({github_cuentas[0]})')
        st.markdown(
            f'Hector Gomez T. Torres - Github [@LornartheBreton]({github_cuentas[1]})')
        st.markdown(
            f'Juan Humberto Escalona Santiago - Github: [@Juanes8]({github_cuentas[2]})')

