import boto3
import json
import datetime

# start the model
def start_model(project_arn,
                model_arn,
                min_inference_units,
                region):
    try:
        client = boto3.client('rekognition', region_name=region)
        # Start the model
        print('Starting model: ' + model_arn)
        response = client.start_project_version(ProjectVersionArn=model_arn, MinInferenceUnits=min_inference_units)
    except Exception as e:
        print(e)

if __name__=='__main__':
    try:
        # Opening JSON file
        f = open('src/crop/trained_model_variables.json')
        # returns JSON object as a dictionary
        data = json.load(f)
        # save manifest variables
        rekog_proj_arn = data['rekog_proj_arn']
        rekog_model_arn = data['rekog_model_arn']
    finally:
        f.close()
    region = "us-east-1"
    min_inference_units = 1
    start_model(rekog_proj_arn, rekog_model_arn, min_inference_units, region)
