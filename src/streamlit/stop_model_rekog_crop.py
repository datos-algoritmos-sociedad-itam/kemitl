import boto3
import json
import datetime

def stop_model(model_arn, region):
    client = boto3.client('rekognition', region_name=region)
    print('Stopping model: ', model_arn)
    # Stop the model
    try:
        response = client.stop_project_version(ProjectVersionArn=model_arn)
        status = response['Status']
        print('Status: ', status)
    except Exception as e:
            print(e)


if __name__=='__main__':
    try:
        # Opening JSON file
        f = open('src/crop/trained_model_variables.json')
        # returns JSON object as a dictionary
        data = json.load(f)
        # save manifest variables
        rekog_proj_arn = data['rekog_proj_arn']
        rekog_model_arn = data['rekog_model_arn']
    finally:
        f.close()

    region = "us-east-1"

    stop_model(rekog_model_arn, region)
