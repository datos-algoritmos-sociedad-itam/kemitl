Nuestros objetivos a corto plazo son los siguientes:
1. Construir una base de datos de imágenes cuyas etiquetas correspondan a pueblos originarios, esta base de datos la obtendremos a través de web scrapping en Google Imágenes dado que no existe una base de datos disponible con las características que necesitamos.
2. Construir una herramienta que, de forma automática, pueda detectar prendas que tengan inspiración clara en patrones textiles de pueblos originarios. Podemos aproximarnos a una solución a través de la clasificación de las imágenes; el objetivo es que, dada una imagen de una prenda, podamos analizarla y detectar si tiene patrones similares a los que tenemos en nuestra base de datos de pueblos originarios.

