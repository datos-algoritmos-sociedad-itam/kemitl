Kemitl
==============================

Análisis de imágenes relacionadas con patrones textiles con diseños de pueblos originarios con la finalidad de identificar su uso desleal o no autorizado por parte de retailers de fast y high fashion, y la subsecuente comunicación de este match y su diferencia de precio mediante un Twitter Bot.

**Partner: [SocialTIC](https://socialtic.org)**

Justificación
-------------

[Según la revista "Fashion Network"](https://mx.fashionnetwork.com/news/Las-ventas-textiles-caen-un-70-en-mexico-debido-al-coronavirus,1201681.html), el mercado de textiles en México vale 30mmdd, de los cuales 6mmdd son atribuidos a exportaciones. La misma industria genera 500 mil empleos de forma directa desde su manufactura, con una proporción de 70% de estos empleos ocupados por mujeres. En números pre-pandemia, estamos hablando de 10% del PIB Mexicano.

En contraste, existen muy pocos datos de lo que le corresponde a las artesanías textiles de estas cantidades, excepto por la cantidad de personas que se dedican a esta actividad, que son 12 millones, es decir 12 veces más que la actividad textil industrial. Pese a esta diferencia, la proporción de mujeres que participan en la manufactura de prendas consideradas como artesanía se mantiene en 70%, al igual que las de fabricación industrial.

[Entre las amenazas principales al mercado de artesanías mexicanas](http://xpatcapital.com/el-mercado-de-artesanias-en-mexico-un-gigantesco-potencial-en-riesgo/) se encuentran, según la consultora de negocios Rosalba Gutiérrez:

1.  Agotamiento de recursos naturales, sobre todo aquellos que sirven de insumo para la manufactura de artesanías.

2.  Artesanías pirata provenientes de China, que ahora terminan en retailers de fast y high fashion cuyos insumos provienen de dicho país

3.  Acceso limitado a procesos de comercialización y exportación

4.  Falta de protección legal y acceso a patentes y registro de autor

5.  Regateo

Planteamiento de la solución
----------------------------

Buscamos desarrollar un mecanismo que, con ayuda de la Ciencia de Datos, nos permita abordar de forma automática soluciones a los retos 2 y 5:

-   El reto 2 lo abordaremos detectando prendas en estos retailers que tengan inspiración clara en patrones textiles de pueblos originarios y que no estén amparados de ningún convenio o acuerdo internacional, sea colectivo o individual.

-   El reto 5 lo abordaremos comunicando el precio (que es parte de la metadata) por el cual se comercializa el artículo de la tienda de high o fast fashion VS el precio ofertado por el artesano o colectivo de artesanos en el contenido de posts de TW con el hashtag #NoAlRegateo.

Actividades
-----------

1.  Definición de criterios de diseños en textiles a analizar

2.  Definición de espacios digitales de uso desleal y fuentes de diseños de patrones de pueblos originarios a analizar

3.  Descarga y documentación de metadatos de imágenes de patrones textiles de pueblos originarios

4.  Construcción de base de datos de imgs y metadatos de patrones textiles de pueblos originarios

5.  Análisis descriptivo de imagenes y sus metadatos

6.  Desarrollo de web scrapper para descarga y etiquetado de imagenes de tiendas de fast y high fashion

7.  Desarrollo de modelo de Computer Vision y pruebas

8.  Desarrollo de Twitter Bot mediante el cual se publicarán las similitudes y diferencias entre lo guardado en base de datos como patrón textil de pueblo originario y patrón textil encontrado en retailers de moda

9.  Pruebas del Twitter Bot

10. Despliegue a producción

Definiciones y criterios
------------------------

### Qué es un patrón textil de un pueblo originario?

Es el tramado de textiles, independientemente del material o su composición y método de manufactura, que haya sido generado por la inspiración o tradición oral o escrita de un grupo que sea parte de un pueblo originario, o uno de sus miembros reconocidos.

### Qué consideramos que es apropiación o uso desleal?

La comercialización de prendas textiles o accesorios a través de canales formales de retail que formen parte de una indumentaria cuyo estampado, diseño y/o elementos gráficos estén claramente inspirados en el trabajo de grupos miembros de pueblos originarios y que dicho uso esté fuera del marco legal de un acuerdo colectivo o individual firmado antes de Junio de 2021.

### Cómo se establece la similitud entre un patrón textil y una prenda comercial?

Esto lo determina un algoritmo de visión por computadora. Se transparentaran los criterios utilizados al momento de finalizar el desarrollo de dicho algoritmo.

Recursos
--------

1.  [Guía breve para distinguir el arte textil de indígenas mexicanos (adn40.mx)](https://www.adn40.mx/noticia/cultura/notas/2017-05-14-08-00/guia-breve-para-distinguir-el-arte-textil-de-indigenas-mexicanos)

2.  [La Jornada - Iniciativa para proteger de plagios al arte textil indígena](https://www.jornada.com.mx/notas/2021/08/10/cultura/iniciativa-para-proteger-de-plagios-al-arte-textil-indigena/)

3.  [México pide a Zara y otras marcas explicar 'apropiación cultural' de textiles (animalpolitico.com)](https://www.animalpolitico.com/2021/05/mpexico-pide-explicacion-zara-marcas-apropiacion-cultural/)

4.  [La acusación a Nike, Louis Vuitton y Oysho: la delgada línea entre la inspiración y la apropiación cultural | EL PAÍS México (elpais.com)](https://elpais.com/mexico/2021-07-04/la-delgada-linea-entre-la-inspiracion-y-la-apropiacion-cultural.html)

5.  [SC creá ley que protege de plagios al arte textil indígena (ciudadania-express.com)](https://www.ciudadania-express.com/2021/igualdad/sc-crea-ley-que-protege-de-plagios-al-arte-textil-indigena-)

Equipo
------

-   Quién lleva el kanban board (PM)? 

-   Quién le dará principalmente a la tecla (Data Scientists / Data Engineer)?

-   Quienes le darán a la tecla part-time (Data Scientists / Data Engineers)?

-   Quién revisa que la tecla esté bien (Mentor Técnico)?

-   Quién revisa que la tecla se apegue al problem domain (Business Specialist)?

Organización del proyecto
------------

**Es importantísimo seguir esta estructura al pie de la letra** para que el equipo de project management pueda dar seguimiento repetible y reproducible a todos los proyectos.


    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

