CREATE TABLE "entities" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "country_id" int,
  "type_id" int,
  "website" varchar,
  "contact" varchar
);

CREATE TABLE "sources" (
  "id" int PRIMARY KEY,
  "name" varchar,
  "type_id" int,
  "country_id" int,
  "website" varchar,
  "contact" varchar
);

CREATE TABLE "countries" (
  "country_id" int PRIMARY KEY,
  "name" varchar,
  "continent_name" varchar
);

CREATE TABLE "types" (
  "id" int PRIMARY KEY,
  "name" varchar,
  "description" varchar
);

CREATE TABLE "patterns" (
  "id" int PRIMARY KEY,
  "name" varchar,
  "sample_url" varchar,
  "culture_id" int,
  "author" varchar,
  "author_contact" varchar,
  "source_id" int
);

CREATE TABLE "offenses" (
  "id" int PRIMARY KEY,
  "evidence_url" varchar,
  "offending_entity_id" int,
  "pattern_id" int,
  "comments" varchar,
  "author_contact" varchar,
  "region_id" int NOT NULL,
  "source_id" int NOT NULL,
  "publish_timestamp" datetime,
  "discovery_timestamp" datetime DEFAULT (now())
);

CREATE TABLE "outcomes" (
  "id" int PRIMARY KEY,
  "offense_id" int,
  "status" varchar,
  "comments" varchar,
  "is_active" bool,
  "update_timestamp" datetime
);

CREATE TABLE "cultures" (
  "id" int PRIMARY KEY,
  "culture_name" varchar,
  "description" varchar
);

CREATE TABLE "regions" (
  "id" int,
  "name" varchar,
  "country_id" int
);

CREATE TABLE "culture_regions" (
  "id" int PRIMARY KEY,
  "culture_id" int,
  "region_id" int
);

ALTER TABLE "countries" ADD FOREIGN KEY ("country_id") REFERENCES "entities" ("country_id");

ALTER TABLE "types" ADD FOREIGN KEY ("id") REFERENCES "entities" ("type_id");

ALTER TABLE "types" ADD FOREIGN KEY ("id") REFERENCES "sources" ("type_id");

ALTER TABLE "countries" ADD FOREIGN KEY ("country_id") REFERENCES "sources" ("country_id");

ALTER TABLE "cultures" ADD FOREIGN KEY ("id") REFERENCES "patterns" ("culture_id");

ALTER TABLE "sources" ADD FOREIGN KEY ("id") REFERENCES "patterns" ("source_id");

ALTER TABLE "entities" ADD FOREIGN KEY ("id") REFERENCES "offenses" ("offending_entity_id");

ALTER TABLE "patterns" ADD FOREIGN KEY ("id") REFERENCES "offenses" ("pattern_id");

ALTER TABLE "offenses" ADD FOREIGN KEY ("id") REFERENCES "outcomes" ("offense_id");

ALTER TABLE "cultures" ADD FOREIGN KEY ("id") REFERENCES "culture_regions" ("culture_id");

ALTER TABLE "regions" ADD FOREIGN KEY ("id") REFERENCES "culture_regions" ("region_id");
