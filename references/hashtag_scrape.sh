#!/bin/bash

$(minet twitter scrape tweets "#$1 filter:images" --limit 500 > tweets_$1.csv)

minet fetch media_urls tweets_$1.csv   --separator "|"   -d images_$1   --throttle 0   --domain-parallelism 5 > report_$1.csv
