# -*- coding: utf-8 -*-
"""
Created on Sat Jul 18 13:01:02 2020

@author: OHyic
"""
# import selenium drivers
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

# import helper libraries
import time
import urllib.request
import os
import requests
import io
from PIL import Image

# custom patch libraries
import patch


class GoogleImageScraper:
    def __init__(
        self,
        webdriver_path,
        image_path,
        search_key="cat",
        number_of_images=1,
        number_of_related_images=0,
        headless=False,
        min_resolution=(0, 0),
        max_resolution=(1920, 1080),
    ):
        # check parameter types

        if type(number_of_images) != int:
            print("[Error] Number of images must be integer value.")
            return

        if not os.path.exists(image_path):
            print("[INFO] Image path not found. Creating a new folder.")
            os.makedirs(image_path)

        options = Options()

        #options.add_argument("--no-sandbox")
        #if headless:
        #    options.add_argument("--headless")

        driver = webdriver.Chrome(executable_path=webdriver_path, chrome_options=options)
        driver.maximize_window()
        driver.get("https://www.google.com")
        self.driver = driver
        self.search_key = search_key
        self.number_of_images = number_of_images
        self.webdriver_path = webdriver_path
        self.image_path = image_path
        print(self.image_path)
        self.url = (
            "https://www.google.com/search?q=%s&source=lnms&tbm=isch&sa=X&ved=2ahUKEwie44_AnqLpAhUhBWMBHUFGD90Q_AUoAXoECBUQAw&biw=1920&bih=947"
            % (search_key)
        )
        self.headless = headless
        self.min_resolution = min_resolution
        self.max_resolution = max_resolution

    def find_image_urls(self,image_enrichment=False,alt_url='',enriched_count=0):
        """
        This function search and return a list of image urls based on the search key.
        Example:
            google_image_scraper = GoogleImageScraper("webdriver_path","image_path","search_key",number_of_photos)
            image_urls = google_image_scraper.find_image_urls()

        """
        print("[INFO] Scraping for image link... Please wait.")
        image_urls = []
        count = 0
        missed_count = 0
        self.driver.get(self.url)
        time.sleep(5)
        indx = 1
        while self.number_of_images > count:
            try:
                # find and click image
                imgurl = self.driver.find_element_by_xpath(
                    '//*[@id="islrg"]/div[1]/div[%s]/a[1]/div[1]/img' % (str(indx))
                )
                imgurl.click()
                missed_count = 0
            except Exception:
                # print("[-] Unable to click this photo.")
                missed_count = missed_count + 1
                if missed_count > 10:
                    print("[INFO] No more photos.")
                    break

            try:
                # select image from the popup
                time.sleep(1)
                class_names = ['n3VNCb']
                #class_names = ["n3VNCb","rg_i Q4LuWd"]
                images = [
                    self.driver.find_elements_by_class_name(class_name)
                    for class_name in class_names
                    if len(self.driver.find_elements_by_class_name(class_name)) != 0
                ][0]
                for image in images:
                    # only download images that starts with http
                    if image.get_attribute("src")[:4].lower() in ["http"]:
                        print("[INFO] %d. %s" % (count, image.get_attribute("src")))
                        image_urls.append(image.get_attribute("src"))
                        count += 1
                        break
                time.sleep(1)
                if image_enrichment and enriched_count>0:
                    xpath_related='//a[@aria-label="See more Related images"]'
                    related_images = self.driver.find_element_by_xpath(xpath_related)
                    related_images.click()
                    nested_count = 0
                    nested_missed_count=0
                    nested_indx=1
                    while enriched_count > nested_count:
                        try:
                            # find and click image
                            imgurl_2 = self.driver.find_element_by_xpath(
                                '//*[@id="islrg"]/div[1]/div[%s]/a[1]/div[1]/img' % (str(nested_indx))
                            )
                            imgurl_2.click()
                            nested_missed_count = 0
                        except Exception:
                            # print("[-] Unable to click this photo.")
                            nested_missed_count = nested_missed_count + 1
                            if nested_missed_count > 10:
                                print("[INFO] No more enriched photos.")
                                break

                        try:
                            # select image from the popup
                            time.sleep(1)
                            class_names = ['n3VNCb']
                            #class_names = ["n3VNCb","rg_i Q4LuWd"]
                            images_2 = [
                                self.driver.find_elements_by_class_name(class_name)
                                for class_name in class_names
                                if len(self.driver.find_elements_by_class_name(class_name)) != 0
                            ][0]
                            for image_2 in images_2:
                                # only download images that starts with http
                                if image_2.get_attribute("src")[:4].lower() in ["http"]:
                                    print("[INFO] NESTED %d_%d. %s" % (count,nested_count, image_2.get_attribute("src")))
                                    image_urls.append(image_2.get_attribute("src"))
                                    nested_count += 1
                                    break
                            time.sleep(1)           
                        except Exception as e:
                            print("[INFO]NESTED  Unable to get link: ",e)
                        try:
                # scroll page to load next image
                            if nested_count % 3 == 0:
                                self.driver.execute_script(
                                    "window.scrollTo(0, " + str(nested_indx * 60) + ");"
                                )
                            element = self.driver.find_element_by_class_name("mye4qd")
                            element.click()
                            print("[INFO] Loading more photos")
                            time.sleep(5)
                        except Exception:
                            time.sleep(1)
                        nested_indx += 1
                    self.driver.get(self.url)

            except Exception as e:
                print("[INFO] Unable to get link: ",e)

            try:
                # scroll page to load next image
                if count % 3 == 0:
                    self.driver.execute_script(
                        "window.scrollTo(0, " + str(indx * 60) + ");"
                    )
                element = self.driver.find_element_by_class_name("mye4qd")
                element.click()
                print("[INFO] Loading more photos")
                time.sleep(5)
            except Exception:
                time.sleep(1)
            indx += 1

        self.driver.quit()
        print("[INFO] Google search ended")
        return image_urls

    def save_images(self,image_urls):
            #save images into file directory
            """
                This function takes in an array of image urls and save it into the prescribed image path/directory.
                Example:
                    google_image_scraper = GoogleImageScraper("webdriver_path","image_path","search_key",number_of_photos)
                    image_urls=["https://example_1.jpg","https://example_2.jpg"]
                    google_image_scraper.save_images(image_urls)
                    
            """
            print("[INFO] Saving Image... Please wait...")
            for indx,image_url in enumerate(image_urls):
                try:
                    print("[INFO] Image url:%s"%(image_url))
                    search_string = ''.join(e for e in self.search_key if e.isalnum())
                    image = requests.get(image_url,timeout=5)
                    if image.status_code == 200:
                        with Image.open(io.BytesIO(image.content)) as image_from_web:
                            try:
                                filename = "%s%s.%s"%(search_string,str(indx),image_from_web.format.lower())
                                image_path = os.path.join(self.image_path, filename)
                                print("[INFO] %d .Image saved at: %s"%(indx,image_path))
                                image_from_web.save(image_path)
                            except OSError:
                                rgb_im = image_from_web.convert('RGB')
                                rgb_im.save(image_path)
                            
                            image_from_web.close()
                except Exception as e:
                    print("[ERROR] Failed to be downloaded",e)
                    pass
            print("[INFO] Download Completed. Please note that some photos are not downloaded as it is not in the right format (e.g. jpg, jpeg, png)")
'''
    def save_images(self, image_urls):
        # save images into file directory
        """
        This function takes in an array of image urls and save it into the prescribed image path/directory.
        Example:
            google_image_scraper = GoogleImageScraper("webdriver_path","image_path","search_key",number_of_photos)
            image_urls=["https://example_1.jpg","https://example_2.jpg"]
            google_image_scraper.save_images(image_urls)

        """
        print("[INFO] Saving Image... Please wait...")
        for indx,image_url in enumerate(image_urls):
            try:
                print("[INFO] Image url:%s"%(image_url))
                search_string = ''.join(e for e in self.search_key if e.isalnum())
                image = requests.get(image_url,timeout=5)
                if image.status_code == 200:
                    with Image.open(io.BytesIO(image.content)) as image_from_web:
                        try:
                            filename = "%s%s.%s"%(search_string,str(indx),image_from_web.format.lower())
                            image_path = os.path.join(self.image_path, filename)
                            print("[INFO] %d .Image saved at: %s"%(indx,image_path))
                            image_from_web.save(image_path)
                        except OSError:
                            rgb_im = image_from_web.convert('RGB')
                            rgb_im.save(image_path)
                        image_resolution = image_from_web.size
                        if image_resolution != None:
                            if image_resolution[0]<self.min_resolution[0] or image_resolution[1]<self.min_resolution[1] or image_resolution[0]>self.max_resolution[0] or image_resolution[1]>self.max_resolution[1]:
                                image_from_web.close()
                                #print("GoogleImageScraper Notification: %s did not meet resolution requirements."%(image_url))
                                os.remove(image_path)

                        image_from_web.close()
            except Exception as e:
                print("[ERROR] Failed to be downloaded",e)
                pass
        print("[INFO] Download Completed. Please note that some photos are not downloaded as it is not in the right format (e.g. jpg, jpeg, png)")
        print("[INFO] Saving Image... Please wait...")
        for indx, image_url in enumerate(image_urls):
            try:
                print("[INFO] Image url:%s" % (image_url))
                search_string = "".join(e for e in self.search_key if e.isalnum())
                image = requests.get(image_url, timeout=5)
                print(f'--- IMAGE STATUS CODE: {image.status_code}')
                if image.status_code == 200:
                    with Image.open(io.BytesIO(image.content)) as image_from_web:
                        try:
                            filename = "%s%s.%s" % (
                                search_string,
                                str(indx),
                                image_from_web.format.lower(),
                            )
                            image_path = os.path.join(self.image_path, filename)
                            print(self.image_path)
                            image_path = self.image_path + '/' + filename
                            image_from_web.save(image_path)
                            print("[INFO] %d .Image saved at: %s" % (indx, image_path))
                        #except OSError:
                        #    rgb_im = image_from_web.convert("RGB")
                        #    rgb_im.save(image_path)
                        image_resolution = image_from_web.size
                        if image_resolution != None:
                            if (
                                image_resolution[0] < self.min_resolution[0]
                                or image_resolution[1] < self.min_resolution[1]
                                or image_resolution[0] > self.max_resolution[0]
                                or image_resolution[1] > self.max_resolution[1]
                            ):
                                image_from_web.close()
                                #print("GoogleImageScraper Notification: %s did not meet resolution requirements."%(image_url))
                                os.remove(image_path)

                        image_from_web.close()
                else:
                    raise Exception("Image status code different NOT 200.")
            except Exception as e:
                print("[ERROR] Failed to be downloaded", e)
                pass
        print(
            "[INFO] Download Completed. Please note that some photos are not downloaded as it is not in the right format (e.g. jpg, jpeg, png)"
        )
'''