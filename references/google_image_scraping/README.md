# Kemitl

Código para scrapear imágenes de Google Images.  
Lee una lista de pueblos (que se puede encontrar en el archivo pueblosUTF.csv, compilada por @Rebeca98, busca los términos en Google Images, y los descarga a la carpeta photos.  

Ahorita, si lo corres, hará la concatenación de los nombres de los pueblos y de términos como "tela" y "textil", los busca en Google Images 
(ej. "Amuzgo textil", "Nuahua telar"), y los descarga en la carpeta photos.  

#  La Librería: Google Image Scraper
Script basado en la librería creada por [ohyicong](https://github.com/ohyicong/Google-Image-Scraper). Es un wrapper alrededor de Selenium.
