# -*- coding: utf-8 -*-
"""
Script para scrapear imágenes de Google Images a partir de un CSV 
utilizando la librería GoogleImageScrapper encontrada en 
https://github.com/ohyicong/Google-Image-Scraper .
"""
# Import libraries
from GoogleImageScrapper import GoogleImageScraper
import os,csv,sys,time

### Parameter definition
"""
SEARCH_ITEMS_CSV_PATH 
    Location of the csv file with the search items.
KEYWORDS (List of Strings)
    List of keywords to generate the combinations with SEARCH_ITEMS_CSV_PATH collection (every element
    in the last one will be joined with the first one).
IMAGE_DIR_PATH (String)
    Directory where the scrapped images will be saved.
WEBDRIVER_PATH (String)
    Directory of the webdriver.
NUMBER_OF_IMAGES (Int)
    Number of images to download, per search term.
HEADLESS (Boolean)
    A weird-ass Chromium parameter. No idea what it does. Keep as False.
MIN_RESOLUTION (Tuple of ints)
    Minimmum resolution of the images to download.
MAX_RESOLUTION (Tuple of ints)
    Maximmum resolution fo the images to download.
"""
SEARCH_ITEMS_CSV_PATH = "./pueblosUTF.csv"
KEYWORDS = ["patron textil"]#["textil", "prenda", "telar", "diseño"]
WEBDRIVER_PATH = os.path.normpath(os.getcwd() + "/webdriver/chromedriver")
IMAGE_PATH = os.path.normpath(os.getcwd() + "/photos")
NUMBER_OF_IMAGES = 1
NUMBER_OF_ENRICHED_IMAGES = 30
HEADLESS = False
MIN_RESOLUTION = (0, 0)
MAX_RESOLUTION = (9999, 9999)

if len(sys.argv) > 1:
    NUMBER_OF_IMAGES = int(sys.argv[1])

### Main program

print("Starting download")

with open(SEARCH_ITEMS_CSV_PATH, newline='',encoding='utf-8') as csv_file:
    reader = csv.reader(csv_file)
    search_items = [item for sublist in list(reader)[1:] for item in sublist]
del reader

search_terms = dict.fromkeys(search_items,KEYWORDS) 
images_to_download = str(len(search_terms)*NUMBER_OF_IMAGES*len(KEYWORDS)*NUMBER_OF_ENRICHED_IMAGES)

print("Aproximately, " + 
    str(images_to_download) + 
    " images will be downloaded")
print("Which correspond to " + str(len(search_terms)) + " search terms")
print("And " + str(len(KEYWORDS)) + " keywords")
print("Downloading " + str(NUMBER_OF_IMAGES*NUMBER_OF_ENRICHED_IMAGES) + " images per search term + keyword combination")

start = input("Start the download? Input 'y' to start the script, or anything else to exit.\n> ")
'''
image_scrapper = GoogleImageScraper(
            WEBDRIVER_PATH,
            IMAGE_PATH,
            "patron amuzgo",
            5,
            HEADLESS,
            MIN_RESOLUTION,
            MAX_RESOLUTION,
        )
        
image_urls = image_scrapper.find_image_urls()
print(image_urls)
image_scrapper.save_images(image_urls)
'''
if start == 'y':
    start = time.time()

    for term in search_terms:
     save_path = IMAGE_PATH + "/" + term

     for keyword in search_terms[term]:

        image_scrapper = GoogleImageScraper(
            WEBDRIVER_PATH,
            save_path,
            term + " " + keyword,
            NUMBER_OF_IMAGES,
            HEADLESS,
            MIN_RESOLUTION,
            MAX_RESOLUTION,
        )
        
        image_urls = image_scrapper.find_image_urls(image_enrichment=True,
            enriched_count=NUMBER_OF_ENRICHED_IMAGES)
        final_urls = list(set(image_urls))
        image_scrapper.save_images(final_urls)

    end = time.time()
    elapsed_time = end-start

    print("All Done!\n\tDownloaded " + str(images_to_download) + "in " + str(elapsed_time) + "seconds." )
else:
    print("Bye!")