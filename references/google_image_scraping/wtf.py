from GoogleImageScrapper import GoogleImageScraper
import os,csv,sys,time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
SEARCH_ITEMS_CSV_PATH = "./pueblosUTF.csv"
KEYWORDS = ["patron amuzgo"]#["textil", "prenda", "telar", "diseño"]
WEBDRIVER_PATH = os.path.normpath(os.getcwd() + "/webdriver/chromedriver")
IMAGE_PATH = os.path.normpath(os.getcwd() + "/photos_test")
NUMBER_OF_IMAGES = 5
HEADLESS = False
MIN_RESOLUTION = (0, 0)
MAX_RESOLUTION = (9999, 9999)
test_url = ['https://static.remove.bg/remove-bg-web/e8d7386fc7db47b8605d512499d134d40de5f0a5/assets/start_remove-c851bdf8d3127a24e2d137a55b1b427378cd17385b01aec6e59d5d4b5f39d2ec.png']
### Main program

options = Options()

#options.add_argument("--no-sandbox")
#options.add_argument("--headless")

image_scrapper = GoogleImageScraper(
            WEBDRIVER_PATH,
            IMAGE_PATH,
            "cat",
            5,
            HEADLESS,
            MIN_RESOLUTION,
            MAX_RESOLUTION,
        )
        
urls=image_scrapper.find_image_urls(image_enrichment=True,enriched_count=2)
final_urls = list(set(urls))
image_scrapper.save_images(final_urls)
#driver = webdriver.Chrome(executable_path=WEBDRIVER_PATH,options=options)
#driver.maximize_window()
#driver.get("https://www.google.com")
