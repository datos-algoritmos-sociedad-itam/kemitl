# Diagrama Entidad Relación

```sql
Table entities as U {
  id int [pk, increment] // auto-increment
  name varchar
  country_id int [ref: < countries.country_id]
  type_id int [ref: < types.id]
  website varchar
  contact varchar
}

Table sources {
  id int [pk]
  name varchar
  type_id int [ref: < types.id]
  country_id int [ref: < countries.country_id]
  website varchar
  contact varchar
}

Table countries {
  country_id int [pk]
  name varchar
  continent_name varchar
 }

Table types {
  id int [pk]
  name varchar
  description varchar
 }

Table patterns {
  id int [pk]
  descripcion varchar
  metada json
  sample_url varchar
  culture_id int [ref: < cultures.id]
  author varchar
  author_contact varchar
  source_id int [ref: < sources.id]
}

Table images_to_analyze {
  id int [pk]
  entity_id varchar [ref: < entities.id]
  metadata json
}

Table offenses {
  id int [pk]
  evidence_url varchar
  offending_entity_id int [ref: < entities.id]
  pattern_id int [ref: < patterns.id]
  aditional_information json
  author_contact varchar
  publish_timestamp datetime
  discovery_timestamp datetime [default: `now()`]
}
Table outcomes {
  id int [pk]
  offense_id int [ref: < offenses.id]
  status varchar
  comments varchar
  is_active bool
  update_timestamp datetime
}

Table cultures {
  id int [pk]
  culture_name varchar
  description varchar
  //admin_id int [ref: > U.id]
}
```
![](figs/Diagrama_Entidad_Relacion_Kemitl.png)


Para editar directamente en dbdiagram.io:
https://dbdiagram.io/d/618fe6d102cf5d186b545f14
